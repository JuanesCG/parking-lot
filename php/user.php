<?php
include 'db.php';

class User extends DB{
    private $nombre;
    private $username;

    // functions that have to do with database usage

    public function userExists($user, $pass){
        // checks if a given user exists

        //$hash = password_hash($pass, PASSWORD_DEFAULT);
        //$this is a reference to the calling object e.g the database
        $query = $this->connect()->prepare('SELECT * FROM usuarios WHERE username = :user'); //AND password = :pass');
        $query->execute(['user' => $user]);//, 'pass' => $hash]);       

        if($query->rowCount()){
            $result = $query->fetch();
            if (password_verify($pass, $result['password'])){
                return true;
            }
        }else{
            return false;
        }
    }

    public function createUser($data){
        // creates a new user and adds it to the database

        $query = $this->connect()->prepare('SELECT * FROM usuarios WHERE username = :user');
        $query->execute(['user' => $data[2]]);

        if($query->rowCount()>=1){
            return False;
        }

        $query = $this->connect()->prepare("INSERT INTO usuarios (nombres,apellidos,username,password, correo) VALUES ( '$data[0]', '$data[1]', '$data[2]', '$data[3]', '$data[4]')");
        $query->execute();
        return True;
    }

    public function changePwd($pwd){
        // changes current user password

        $hash = password_hash($pwd, PASSWORD_DEFAULT);
        $user = $this->username;
        $query = $this->connect()->prepare('UPDATE usuarios SET password = :pass WHERE usuarios.username = :user ');
        $query->execute(['pass'=> $hash, 'user'=> $user]);
    }

    // functions that have to do with current user

    public function setUser($user){
        // finds user and sets $nombre and $username;
        $query = $this->connect()->prepare('SELECT * FROM usuarios WHERE username = :user');
        $query->execute(['user' => $user]);
        
        foreach ($query as $currentUser) {
            $this->nombre = $currentUser['nombres'];
            $this->username = $currentUser['username'];
        }
    }

    public function getNombre(){
        return $this->nombre;
    }
    public function getUsername(){
        return $this->username;
    }
}

?>