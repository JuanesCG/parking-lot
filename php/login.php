<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Iniciar Sesión - Parqueadero </title>
    <link rel="stylesheet" href="styles/stylesLogin.css">
</head>
<body>
    <div class="main"> 
        <form action="" method="POST">
            <?php
                if(isset($errorLogin)){
                    echo $errorLogin;
                }
            ?>
            <h2 class='title'>Iniciar sesión</h2>
            <p>Nombre de usuario: <br>
            <input class="form" type="text" name="username"></p>
            <p>Contraseña: <br>
            <input class="form" type="password" name="password"></p>
            <input class="big-button" type="submit" value="Iniciar Sesión"
                                name ="btnLogin">
            <input class="big-button" type="button" value="Registrarse"
                                name ="btnRegister" id="btnRegister">
        </form>
    </div>
    
    <!-- Registration Form -->
    <div id="reg_Form" class="regForm">

        <!-- Modal content -->
        <div class="reg-content">
        <span class="close">&times;</span>
        
        <form action="" method="POST" class="gridC">
            <div class="itemHr">
                <h2 class='title'>Registrarme</h2>
            </div>
            <div class="item">
                <p>Nombres:
                <input class="form" type="text" name="new_nombres" required></p>
            </div>
            <div class="item">
                <p>Apellidos: 
                <input class="form" type="text" name="new_apellidos" required></p>
            </div>

            <div class="itemHr"><hr></div>

            <div class="item">
                <p>E-mail: 
                <input class="form" type="email" name="new_correo" required></p>
            </div>
            <div class="item">
                <p>Nombre de usuario: 
                <input class="form" type="text" name="new_username" required></p>
            </div>

            <div class="itemHr"><hr></div>

            <div class="item">
                <p>Contraseña: 
                <input class="form" type="password" name="new_password" required></p>
            </div>

            <div class="item">
            <p>Confirmar contraseña:
            <input class="form" type="password" name="new_password_confirm" required></p>
            </div>

            <div class="itemHr"><hr></div>

            <div class="itemHr">
                <input class="big-button" type="submit" value="Crear usuario"
                                        name ="btnReg">
            </div>
        </form>
        </div>

    </div>

    <script>
        // Get the modal
        var modal = document.getElementById("reg_Form");

        // Get the button that opens the modal
        var btn = document.getElementById("btnRegister");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks the button, open the modal 
        btn.onclick = function() {
        modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
        modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
        }
    </script>
    
</body>
</html>