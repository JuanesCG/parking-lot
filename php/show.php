<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Created by: Juan Esteban Castro Guerrero -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="../styles/stylesShow.css">
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
   <script>
   	$(document).ready(function(){
   		$('#form').on('submit',(e)=> {
   			e.preventDefault();
   			
   			var formData = new FormData($('#form')[0]);
   			$.ajax({
   				type: 'POST',
   				url: 'search.php',
   				data: formData,
   				processData: false,
   				contentType: false,
   				cache: false,
   				success: function(msg){
   					console.log(msg)
   					$( "#result" ).remove();
   					$( "#main" ).prepend(msg);
   					$('#form')[0].reset();	
   				}
   			})
   		});
   	});
   </script>
  <title>Show</title>
</head>
<html>
<body>
<a href="../index.php" class="btnHome"> </a>
<div id="main" class="main">
	<!-- "< ?php echo htmlentities($_SERVER['PHP_SELF'])?>" -->

<form id="form" class="gridC" method="post">
	<div class="item">
			Placa del auto a Buscar: 
		</div>
	<div class="item">
		    <input type="text" class="form" name="Sboard" required> 
	</div>
	<div class="item">
			<br><input type="submit" name="submit" value="Buscar" class="big-button">
	</div>
</form>


</div>
</body>
</html> 