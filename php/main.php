<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Created by: Juan Esteban Castro Guerrero -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="styles/styles.css">
  <title>Parking lot</title>
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script>
      $(document).ready(function(){
        $('#myform').on('submit',(e)=> {
          e.preventDefault();
          var formData = new FormData($('#myform')[0]);
          $.ajax({
            type: 'POST',
            url: 'php/changePwd.php',
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success: function(data){
              //console.log(msg)
              $(document.body).empty();
              $(document.body).append(data);
              $('#myform')[0].reset();	
            }
          })
        });
      }); 
  </script>
</head>
<html>
<body>
<div class="title">
<h2>Registro de Parqueadero</h2>
</div>

<!-- Sidenav button -->
<span class="btnOpt" onclick="openNav()"></span>

<!-- main content of page -->
<div class="main">
	<input type="button" value="Registrar auto" class="big-button" id="btnRegister" 
	onClick="document.location.href='php/registry.php'" />
	<span class="vertical-line"></span>
	<input type="button" value="Consultar registro" class="big-button" id="btnConsult" 
	onClick="document.location.href='php/show.php'" />
</div>
<!-- Error from password change form -->

<!-- sidenav -->
<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <p>Bienvenido <br/> <?php echo $user->getNombre(); ?> <hr>  </p>
  
  <a href="#" id="myBtn">Cambiar contraseña</a>
  <a href="#">Mis datos</a>
  <a href="#">Mis registros</a>
  <a href="#">Contacto</a>
</div>

<!-- Logout button -->
<a href="php/logout.php" class="btnExit"> </a>


<!-- Change Password Modal -->
<div id="pwdChange" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <form id= "myform" action="" method="POST">

      <h2 class='title pwd'>Cambio de contraseña</h2>
      <p>Contraseña actual: <br>
      <input class="form" type="password" name="c_password" required></p>
      <p>Nueva Contraseña: <br>
      <input class="form" type="password" name="n_password" required></p>
      <p>Confirmar Nueva Contraseña: <br>
      <input class="form" type="password" name="confirm_n_password" required></p>
      <input class="big-button" type="submit" value="Cambiar Contraseña" name ="btnChangePwd">
      
    </form>
  </div>
</div>

<?php
  if(isset($result)){
    echo $result;
  }
?>

<script>
  // functions for sidenav
  /* Set the width of the side navigation to 250px */
    function openNav() {
      document.getElementById("mySidenav").style.width = "250px";
    }

    /* Set the width of the side navigation to 0 */
    function closeNav() {
      document.getElementById("mySidenav").style.width = "0";
    }

  // functions we already saw for modals
  var modal = document.getElementById("pwdChange");
  var btn = document.getElementById("myBtn");
  var span = document.getElementsByClassName("close")[0];
  btn.onclick = function() {
    modal.style.display = "block";
  }
  span.onclick = function() {
    modal.style.display = "none";
  }
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
      }
    } 
</script>
</body>
</html> 