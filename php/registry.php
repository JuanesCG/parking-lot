<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Created by: Juan Esteban Castro Guerrero -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="../styles/stylesReg.css">
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
   <script>
   	$(document).ready(function(){
   		$('#myform').on('submit',(e)=> {
   			e.preventDefault();
   			var formData = new FormData($('#myform')[0]);
   			$.ajax({
   				type: 'POST',
   				enctype: 'multipart/form-data',
   				url: 'save.php',
   				data: formData,
   				processData: false,
   				contentType: false,
   				cache: false,
   				success: function(msg){
   					//console.log(data)
   					$(document.body).append(msg);
   					$('#myform')[0].reset();	
   				}
   			})
   		});
   	});
   </script>
  <title>Registry</title>
</head>
<html>
<body>
<a href="../index.php" class="btnHome"> </a>
<div class="main">

	<form id="myform" enctype="multipart/form-data" action="save.php" class="gridC" method="post" >
		<div class="item">
			Nombres del propietario: <input type="text" class="form" name="Rname" required> 
		</div>
		<div class="item">
			Apellidos del propietario: <input type="text"class="form" name="Rsurname" required>
		</div>
		<div class="itemHr"><hr></div>
		<div class="item">
			Placa del automóvil: <input type="text" class="form" name="board" required>
		</div>
		<div class="item">
			Fotografía: <input type="file" class="file" name="image" required>
		</div>
		<div class="itemHr"><hr></div>
		<div class="item">
			Color del automóvil:<input name="Color" class="form" type="color" required/>
		</div>
		<div class="item">
			Fecha de ingreso: <input type="date" class="form" name="date" required>
		</div>
		<div class="itemHr"><hr></div>
		<div class="itemHr"><br><input type="submit" name="submit" value="Registrar automóvil" class="big-button"></div>
	</form>
</div>

</body>
</html> 