<?php

	// Create database connection
	$database = mysqli_connect("localhost", "root", "", "registro_autos");
	//vars
	$Rname= $_POST['Rname'];
	$Rsurname= $_POST['Rsurname'];
	$color = $_POST['Color'];
	$board= $_POST['board'];
	$date = $_POST['date'];
	$target_dir = "../uploads/";
	$target_file = $target_dir .$board ."-". $Rsurname . "--" . basename($_FILES["image"]["name"]);
	$uploadOk = 1;
	$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
	// Check if board already exists
	$query = "SELECT * FROM registros WHERE placa='$board'";
	$raw_results = mysqli_query($database, $query);
	if (mysqli_num_rows($raw_results) >= 1){
		echo '
	  <div class="alert">
	    	<span class="closebtn" onclick="this.parentElement.style.display=\'none\';"   >&times;</span> 
	    	La placa especificada ya existe en los registros del parqueadero.
	  </div> ';
	  $uploadOk = 0;
	  exit();
	}
	// Check file size
	if ($_FILES["image"]["size"] > 500000) {
	  echo '
	  <div class="alert">
	    	<span class="closebtn" onclick="this.parentElement.style.display=\'none\';"   >&times;</span> 
	    	El archivo es demasiado grande.
	  </div> ';
	  $uploadOk = 0;
	  exit();
	}

	// Allow certain file formats
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
	&& $imageFileType != "gif" ) {
	  echo "
	<div class=\"alert\">
	    	<span class=\"closebtn\" onclick=\"this.parentElement.style.display='none';\"   >&times;</span> 
	    	Sólo se admiten archivos JPG, JPEG, PNG & GIF
	</div> ";
	  $uploadOk = 0;
	  exit();
	}
	// if everything is ok, try to upload file
	// moving the file to target directory
	move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);

	// saving values to database
	$sql = "INSERT INTO registros (n_propietario,a_propietario,placa,foto,color,fecha_ingreso) VALUES ('$Rname', '$Rsurname','$board','$target_file','$color','$date')";
	// execute query
  	mysqli_query($database, $sql);
  	// alert succesful transaction
  	echo "
	  <div class=\"alert\">
	    	<span class=\"closebtn\" onclick=\"this.parentElement.style.display='none';\"   >&times;</span> 
	    	¡Datos guardados!, desea <a href=\"show.php\">consultar?</a>
	</div> 
	";
?>