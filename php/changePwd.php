<?php


include_once '../index.php';

    // if the change-password-button was clicked inside main.php,
    // attemp to change user's password
    // retrieve data
    $c_pwd = $_POST['c_password'];
    $n_pwd = $_POST['n_password'];
    $confirm_n_pwd = $_POST['confirm_n_password'];
    $username = $user->getUsername();

    // validate password
    if ($user->userExists($username, $c_pwd)){
        if ($n_pwd != $confirm_n_pwd){
            echo "
            <div class=\"alert\">
                    <span class=\"closebtn\" onclick=\"this.parentElement.style.display='none';\"   >&times;</span> 
                    ¡Las contraseñas no coinciden!
            </div> ";
            exit();
        }
        $user->changePwd($n_pwd);
        echo "
        <div class=\"alert\">
                <span class=\"closebtn\" onclick=\"this.parentElement.style.display='none';\"   >&times;</span> 
                ¡Contraseña cambiada correctamente!
        </div> ";
        exit();
        
    }
    echo "
    <div class=\"alert\">
            <span class=\"closebtn\" onclick=\"this.parentElement.style.display='none';\"   >&times;</span> 
            !La contraseña dada no es la correcta!
    </div> ";
    exit();





?>