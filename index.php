<?php

include_once 'php/user.php';
include_once 'php/user_session.php';

$userSession = new UserSession();
$user = new User();
$result = "";

if (isset($_POST['btnReg'])) {
    // on btnReg pressed, validate data, create user and redirect to login.php

    //retrieve data from form 
    $n_nombres = $_POST['new_nombres']; 
    $n_apellidos = $_POST['new_apellidos']; 
    $n_username = $_POST['new_username']; 
    $n_password = $_POST['new_password']; 
    $confirm_pwd = $_POST['new_password_confirm'];
    $n_correo = $_POST['new_correo'];

    //validate password
    if ($n_password != $confirm_pwd){
        $errorLogin = "¡Las contraseñas no coinciden!";
        include_once 'php/login.php';
    }
    //password is correct, try to create user
    else{
        // ecrypt password on a 60 pos. char!
        $hash = password_hash($n_password, PASSWORD_DEFAULT);
        $data = array( $n_nombres,$n_apellidos,$n_username,$hash,$n_correo);

        // if everythin went ok, inform the user and redirect to login.php
        if($user->createUser($data)){
            $errorLogin = "¡usuario registrado!";
            include_once 'php/login.php';
        }
        // if user already exists, abort operation
        else{
            $errorLogin = "!Error!, el usuario ya existe";
            include_once 'php/login.php';
        }
    }
}else if(isset($_SESSION['user'])){
    // if there's a session, redirect to main.php 

    $user->setUser($userSession->getCurrentUser());
    include_once 'php/main.php';

}else if(isset($_POST['username']) && isset($_POST['password'])){
    // if the username and password are set, validate them and create
    // session for current user

    // retrieve username and password from the form 
    $userForm = $_POST['username'];
    $passForm = $_POST['password'];

    // check if user exists
    if($user->userExists($userForm, $passForm)){

        $userSession->setCurrentUser($userForm);
        $user->setUser($userForm);
        include_once 'php/main.php';
    }else{
        // name and/or pwd is incorrect, redirect to login.php
        $errorLogin = "Nombre de usuario y/o contraseña es incorrecto";
        include_once 'php/login.php';
    }

}else{
    // default option, if no data is given by user, simply redirect to login
    include_once 'php/login.php';
}

?>